package it.unibo.trashware.persistence.repository.mapper;

import it.unibo.trashware.persistence.repository.crud.RequestHandler;

/**
 * Marker interface to define the link in the dispatching chain which defines
 * what portion of the subdomain will be managed.
 * <p>
 * At this level the technology is still unknown.
 * 
 * @author Manuel Bonarrigo
 *
 */
public interface PersistenceMapper extends RequestHandler {

}
