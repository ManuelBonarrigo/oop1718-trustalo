/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Identity;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Indexes;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Keys;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Trustalo;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records.DevicerequestdetailsRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Devicerequestdetails extends TableImpl<DevicerequestdetailsRecord> {

    private static final long serialVersionUID = 35852666;

    /**
     * The reference instance of <code>trustalo.devicerequestdetails</code>
     */
    public static final Devicerequestdetails DEVICEREQUESTDETAILS = new Devicerequestdetails();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<DevicerequestdetailsRecord> getRecordType() {
        return DevicerequestdetailsRecord.class;
    }

    /**
     * The column <code>trustalo.devicerequestdetails.ID</code>.
     */
    public final TableField<DevicerequestdetailsRecord, UInteger> ID = createField("ID", org.jooq.impl.SQLDataType.INTEGERUNSIGNED.nullable(false).identity(true), this, "");

    /**
     * The column <code>trustalo.devicerequestdetails.Request</code>.
     */
    public final TableField<DevicerequestdetailsRecord, UInteger> REQUEST = createField("Request", org.jooq.impl.SQLDataType.INTEGERUNSIGNED, this, "");

    /**
     * The column <code>trustalo.devicerequestdetails.Category</code>.
     */
    public final TableField<DevicerequestdetailsRecord, String> CATEGORY = createField("Category", org.jooq.impl.SQLDataType.VARCHAR(10).nullable(false), this, "");

    /**
     * The column <code>trustalo.devicerequestdetails.Annotations</code>.
     */
    public final TableField<DevicerequestdetailsRecord, String> ANNOTATIONS = createField("Annotations", org.jooq.impl.SQLDataType.CLOB, this, "");

    /**
     * The column <code>trustalo.devicerequestdetails.Quantity</code>.
     */
    public final TableField<DevicerequestdetailsRecord, UByte> QUANTITY = createField("Quantity", org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>trustalo.devicerequestdetails.ComponentOfRequestDevice</code>.
     */
    public final TableField<DevicerequestdetailsRecord, UInteger> COMPONENTOFREQUESTDEVICE = createField("ComponentOfRequestDevice", org.jooq.impl.SQLDataType.INTEGERUNSIGNED, this, "");

    /**
     * Create a <code>trustalo.devicerequestdetails</code> table reference
     */
    public Devicerequestdetails() {
        this(DSL.name("devicerequestdetails"), null);
    }

    /**
     * Create an aliased <code>trustalo.devicerequestdetails</code> table reference
     */
    public Devicerequestdetails(String alias) {
        this(DSL.name(alias), DEVICEREQUESTDETAILS);
    }

    /**
     * Create an aliased <code>trustalo.devicerequestdetails</code> table reference
     */
    public Devicerequestdetails(Name alias) {
        this(alias, DEVICEREQUESTDETAILS);
    }

    private Devicerequestdetails(Name alias, Table<DevicerequestdetailsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Devicerequestdetails(Name alias, Table<DevicerequestdetailsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Trustalo.TRUSTALO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.DEVICEREQUESTDETAILS_PRIMARY, Indexes.DEVICEREQUESTDETAILS_REQUESTDEVICESCOMPOUNDFK, Indexes.DEVICEREQUESTDETAILS_REQUESTDEVICESREQUESTCATEGORYUNIQUE, Indexes.DEVICEREQUESTDETAILS_TRASHWAREDEVICECATEGORIESFK_IDX);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identity<DevicerequestdetailsRecord, UInteger> getIdentity() {
        return Keys.IDENTITY_DEVICEREQUESTDETAILS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<DevicerequestdetailsRecord> getPrimaryKey() {
        return Keys.KEY_DEVICEREQUESTDETAILS_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<DevicerequestdetailsRecord>> getKeys() {
        return Arrays.<UniqueKey<DevicerequestdetailsRecord>>asList(Keys.KEY_DEVICEREQUESTDETAILS_PRIMARY, Keys.KEY_DEVICEREQUESTDETAILS_REQUESTDEVICESREQUESTCATEGORYUNIQUE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<DevicerequestdetailsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<DevicerequestdetailsRecord, ?>>asList(Keys.REQUESTDEVICESREQUESTFK, Keys.TRASHWAREDEVICECATEGORIESFK, Keys.REQUESTDEVICESCOMPONENTOFREQUESTDEVICEFK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Devicerequestdetails as(String alias) {
        return new Devicerequestdetails(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Devicerequestdetails as(Name alias) {
        return new Devicerequestdetails(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Devicerequestdetails rename(String name) {
        return new Devicerequestdetails(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Devicerequestdetails rename(Name name) {
        return new Devicerequestdetails(name, null);
    }
}
