/*
 * This file is generated by jOOQ.
*/
package it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables;


import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Index;
import org.jooq.Name;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.DSL;
import org.jooq.impl.TableImpl;
import org.jooq.types.UByte;
import org.jooq.types.UInteger;

import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Indexes;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Keys;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.Trustalo;
import it.unibo.trashware.persistence.repository.mapper.jooq.trustalodb.trustalo.tables.records.PrintermodelsRecord;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.6"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Printermodels extends TableImpl<PrintermodelsRecord> {

    private static final long serialVersionUID = -1296440223;

    /**
     * The reference instance of <code>trustalo.printermodels</code>
     */
    public static final Printermodels PRINTERMODELS = new Printermodels();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<PrintermodelsRecord> getRecordType() {
        return PrintermodelsRecord.class;
    }

    /**
     * The column <code>trustalo.printermodels.DeviceModel</code>.
     */
    public final TableField<PrintermodelsRecord, UInteger> DEVICEMODEL = createField("DeviceModel", org.jooq.impl.SQLDataType.INTEGERUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>trustalo.printermodels.Tecnology</code>.
     */
    public final TableField<PrintermodelsRecord, UByte> TECNOLOGY = createField("Tecnology", org.jooq.impl.SQLDataType.TINYINTUNSIGNED.nullable(false), this, "");

    /**
     * The column <code>trustalo.printermodels.Resolution</code>.
     */
    public final TableField<PrintermodelsRecord, Short> RESOLUTION = createField("Resolution", org.jooq.impl.SQLDataType.SMALLINT, this, "");

    /**
     * Create a <code>trustalo.printermodels</code> table reference
     */
    public Printermodels() {
        this(DSL.name("printermodels"), null);
    }

    /**
     * Create an aliased <code>trustalo.printermodels</code> table reference
     */
    public Printermodels(String alias) {
        this(DSL.name(alias), PRINTERMODELS);
    }

    /**
     * Create an aliased <code>trustalo.printermodels</code> table reference
     */
    public Printermodels(Name alias) {
        this(alias, PRINTERMODELS);
    }

    private Printermodels(Name alias, Table<PrintermodelsRecord> aliased) {
        this(alias, aliased, null);
    }

    private Printermodels(Name alias, Table<PrintermodelsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return Trustalo.TRUSTALO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Index> getIndexes() {
        return Arrays.<Index>asList(Indexes.PRINTERMODELS_PRIMARY, Indexes.PRINTERMODELS_PRINTERMODELSTECNOLOGYFK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<PrintermodelsRecord> getPrimaryKey() {
        return Keys.KEY_PRINTERMODELS_PRIMARY;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<PrintermodelsRecord>> getKeys() {
        return Arrays.<UniqueKey<PrintermodelsRecord>>asList(Keys.KEY_PRINTERMODELS_PRIMARY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<PrintermodelsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<PrintermodelsRecord, ?>>asList(Keys.PRINTERMODELSDEVICEMODELFK, Keys.PRINTERMODELSTECNOLOGYFK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Printermodels as(String alias) {
        return new Printermodels(DSL.name(alias), this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Printermodels as(Name alias) {
        return new Printermodels(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public Printermodels rename(String name) {
        return new Printermodels(DSL.name(name), null);
    }

    /**
     * Rename this table
     */
    @Override
    public Printermodels rename(Name name) {
        return new Printermodels(name, null);
    }
}
