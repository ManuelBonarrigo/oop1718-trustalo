package it.unibo.trashware.persistence.repository;

import it.unibo.trashware.persistence.repository.crud.domain.CrudDevices;
import it.unibo.trashware.persistence.repository.crud.domain.CrudIntel;
import it.unibo.trashware.persistence.repository.crud.domain.CrudPeople;
import it.unibo.trashware.persistence.repository.crud.domain.CrudProcessors;
import it.unibo.trashware.persistence.repository.crud.domain.CrudRequests;

/**
 * The interface for the client-based boundary of the persistence module.
 * <p>
 * Through the usage of this interface any client can have access to the
 * capability of making any domain related objects persistent in some way,
 * which the client should not care about, since it is pursued automatically by
 * the module.
 * 
 * @author Manuel Bonarrigo
 *
 */

public interface Persistence extends CrudPeople, CrudDevices, CrudRequests, CrudIntel, CrudProcessors {

}
