package it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy;

import java.util.Set;

import it.unibo.trashware.persistence.model.otherdevices.Processor;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link Processor}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentProcessor {
    /**
     * The CRUD operation of proposing a new {@link Processor} to be created.
     * 
     * @param processor
     *            the Processor to be created.
     * @throws DuplicateKeyValueException
     *             if the Processor to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(Processor processor) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link Processor} filtered
     * by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the Processor objects matched against the filter
     */
    Set<Processor> readProcessors(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link Processor} to be updated with the
     * value of a new one.
     * 
     * @param oldProcessor
     *            the Processor actually stored.
     * @param newProcessor
     *            the Processor with the informations to be fetched for update.
     * @throws DuplicateKeyValueException
     *             if the Processor update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(Processor oldProcessor, Processor newProcessor) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link Processor} for deletion.
     * 
     * @param processor
     *            the Processor to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(Processor processor) throws NonExistentReferenceException, BoundedReferenceException;

}
