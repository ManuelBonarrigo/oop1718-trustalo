package it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy;

import java.util.Set;

import it.unibo.trashware.persistence.model.devices.HardDiskDrive;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link HardDiskDrive}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentHardDiskDrive {
    /**
     * The CRUD operation of proposing a new {@link HardDiskDrive} to be created.
     * 
     * @param hdd
     *            the HardDiskDrive to be created.
     * @throws DuplicateKeyValueException
     *             if the HardDiskDrive to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(HardDiskDrive hdd) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link HardDiskDrive}
     * filtered by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the HardDiskDrive objects matched against the
     *         filter
     */
    Set<HardDiskDrive> readHardDiskDrive(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link HardDiskDrive} to be updated with
     * the value of a new one.
     * 
     * @param oldHdd
     *            the HardDiskDrive actually stored.
     * @param newHdd
     *            the HardDiskDrive with the informations to be fetched for update.
     * @throws DuplicateKeyValueException
     *             if the HardDiskDrive update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(HardDiskDrive oldHdd, HardDiskDrive newHdd) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link HardDiskDrive} for deletion.
     * 
     * @param hdd
     *            the HardDiskDrive to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(HardDiskDrive hdd) throws NonExistentReferenceException, BoundedReferenceException;

}
