package it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy;

import java.util.Set;

import it.unibo.trashware.persistence.model.devices.DigitalInformationUnit;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link DigitalInformationUnit}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentDigitalInformationUnit {
    /**
     * The CRUD operation of proposing a new {@link DigitalInformationUnit} to be
     * created.
     * 
     * @param unit
     *            the DigitalInformationUnit to be created.
     * @throws DuplicateKeyValueException
     *             if the DigitalInformationUnit to be created is already present,
     *             and the persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(DigitalInformationUnit unit) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of
     * {@link DigitalInformationUnit} filtered by the conditions of the
     * {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the DigitalInformationUnit objects matched
     *         against the filter
     */
    Set<DigitalInformationUnit> readDigitalInformationUnit(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link DigitalInformationUnit} to be
     * updated with the value of a new one.
     * 
     * @param oldUnit
     *            the DigitalInformationUnit actually stored.
     * @param newUnit
     *            the DigitalInformationUnit with the informations to be fetched for
     *            update.
     * @throws DuplicateKeyValueException
     *             if the DigitalInformationUnit update transforms the entity in an
     *             already present one, and the persistence storage does not
     *             tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(DigitalInformationUnit oldUnit, DigitalInformationUnit newUnit) 
            throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link DigitalInformationUnit} for
     * deletion.
     * 
     * @param unit
     *            the DigitalInformationUnit to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(DigitalInformationUnit unit) throws NonExistentReferenceException, BoundedReferenceException;

}
