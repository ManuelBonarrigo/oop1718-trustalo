package it.unibo.trashware.persistence.repository.crud.entities.people;

import java.util.Set;

import it.unibo.trashware.persistence.model.people.TrashwareWorker;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link TrashwareWorker}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentTrashwareWorker {

    /**
     * The CRUD operation of proposing a new {@link TrashwareWorker} to be created.
     * 
     * @param worker
     *            the TrashwareWorker to be created.
     * @throws DuplicateKeyValueException
     *             if the TrashwareWorker to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(TrashwareWorker worker) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link TrashwareWorker}
     * filtered by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the TrashwareWorker objects matched against the
     *         filter
     */
    Set<TrashwareWorker> readTrashwareWorkers(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link TrashwareWorker} to be updated with
     * the value of a new one.
     * 
     * @param oldTrashwareWorker
     *            the TrashwareWorker actually stored.
     * @param newTrashwareWorker
     *            the TrashwareWorker with the informations to be fetched for
     *            update.
     * @throws DuplicateKeyValueException
     *             if the TrashwareWorker update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(TrashwareWorker oldTrashwareWorker, TrashwareWorker newTrashwareWorker) 
            throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link TrashwareWorker} for deletion.
     * 
     * @param worker
     *            the TrashwareWorker to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(TrashwareWorker worker) throws NonExistentReferenceException, BoundedReferenceException;
}
