package it.unibo.trashware.persistence.model.test;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

/**
 * A JUnit Suite test invoked over all the packages of the domain model.
 * 
 * @author Manuel Bonarrigo
 *
 */
@RunWith(JUnitPlatform.class)
@SelectPackages({"it.unibo.trashware.persistence.model.test.devices", 
                "it.unibo.trashware.persistence.model.test.otherdevices",
                "it.unibo.trashware.persistence.model.test.people",
                "it.unibo.trashware.persistence.model.test.requests"})
public class JUnit5TestSuiteDomainModelCompleteTest { }
