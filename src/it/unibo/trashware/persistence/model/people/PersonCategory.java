package it.unibo.trashware.persistence.model.people;

import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * This interface describes the category assignable to a
 * {@link JuridicalPerson}.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@InterfaceToSchemaEntity(schemaEntity = "PersonCategories")
public interface PersonCategory extends Comparable<PersonCategory> {

    /**
     * Retrieve the name of such a category.
     * 
     * @return a {@link String} with the name of the category
     */
    @InterfaceMethodToSchemaField(returnType = String.class, schemaField = "Name")
    String getName();

}
